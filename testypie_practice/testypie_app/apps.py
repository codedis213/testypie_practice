from django.apps import AppConfig


class TestypieAppConfig(AppConfig):
    name = 'testypie_app'
